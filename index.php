<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KORA Coaching Group</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/kora.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">
     <!-- NAVIGATION -->
     <?php require 'partials/navigation.php'; ?>

     <?php require 'partials/slideshow.php'; ?>

    <section class="bg-primary no-padding" id="about">
        <?php require 'partials/about.php'; ?>
    </section>
    
    <?php require 'partials/about-kora-founder.php'; ?>
           
           <!-- COACHING SECIONT -->
 	<section class="bg-light -center" id="coaching" >
        <?php require 'partials/coaching.php'; ?>  
    </section>
    
    <!-- END OF COACHING SECTION -->
    <section class="consulting" id="consulting" >
        <?php require 'partials/consulting.php'; ?>
    </section>

 	<section class="bg-light -center" id="courses" style="background:#34495e;color:#fff;">
        <?php require 'partials/courses.php'; ?>
    </section>
    
    <aside class="bg-dark" id="community">
        <?php require 'partials/community.php'; ?>
    </aside>

      <aside class="bg-dark" id="coaches">
        <?php require 'partials/coaches.php'; ?>
    </aside>

    <aside class="bg-dark" id="conference">
        <?php require 'partials/conference.php'; ?>
    </aside>

    <aside class="bg-dark" id="coverage">
        <?php require 'partials/coverage.php'; ?>
    </aside>
    <aside class="bg-dark" id="colleagues">
        <?php require 'partials/colleagues.php'; ?>
    </aside>
    <aside class="bg-dark" id="comments">
        <?php require 'partials/comments.php'; ?>
    </aside>
    <aside class="bg-dark" id="clients">
        <?php require 'partials/clients.php'; ?>
    </aside>

    <section id="contact">
        <?php require 'partials/contact.php'; ?>
    </section>
    
<div id="footer">
      <div class="container">
        <div class="muted credit pull-left">Copyright Kora-coaching.</div>
        <div class="pull-right" style="font-size: 12px;color: #444;">
            <a href="https://www.facebook.com/pages/KORA-Associates/302365949910356"  target="_blank"><img src="img/facebook.png">Facebook</a>
            <a href="https://twitter.com/Kora_Associates" target="_blank"><img src="img/twitter.png">Twitter</a>
            <a href="https://www.youtube.com/channel/UCszccI5PyAMTsNto2_Y6pgA"  target="_blank"><img src="img/youtube.png">Youtube</a>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>

</body>

</html>     