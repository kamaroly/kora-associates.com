<div class="modal fade col-md-12" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog " role="document">
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel" style="color: #444;">About KORA Founder, Group CEO & Coach</h4>
</div>
<div class="modal-body" style="color: #444 !important;">
<div class="row">
  <p style="font-size: 28px;font-weight: 100;">
    <img src="img/mireille-karera.png" class="img" align="left">
“As a Coaching & Coaching Company, we value the input of Human Capital in an organization.
As such, we labor to provide a workspace, conducive to fostering growth of our teams.
Our organization appreciates flexible employment through consultants and outsourced contractors.
Our Associates model enables colleagues to growth, as Entrepreneurs, both internally and externally.
We teach, coach and enable career growth for our team members. To find out more about career opportunities KORA Coaching Group, 
feel free to contact us below.”                                        
  </p>
  <p style="text-align:center;">  – Ineza Mireille KARERA, Group CEO & Coach </p>
 </div>
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>