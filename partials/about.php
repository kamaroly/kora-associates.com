
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="text-align:left;margin-left:420px;">About us</h2>
                    <hr class="light" style="text-align:left;margin-left:450px;">
                    <p>The expression "KORA"  translates into "doing, creating, acting, working,  realizing, 
                    generating, innovating, bringing forth and developing" - all of these attributes perfectly 
                    describing Kora-coaching’ core values.
                    </p>
                    <p>
Kora Coaching Group are business professionals and Entrepreneurs with a combined expertise in excess of 15 years. We have a track record in global business activities ranging from Coaching, Consulting, Advisory, to Business Development in Financial Services, Conferences and Event Management.
</p>

<h4>KORA’s business lines are categorized in three core activities</h4>
<hr class="light">
    <a href="#coaching" class="btn btn-primary" style="background: #34495e !important;">Coaching</a> 
    <a href="#consulting" class="btn btn-success">Consulting</a> 
    <a href="#community" class="btn btn-warning"
     style="background: #ddd;border-color: #ccc;color: #337ab7" 
    >Community</a>

<p>Our core business activities have a common denominator, which is growth enablement for individuals either independently or within a context of an organization. 
Our tagline reflects all aspects of the 3 business lines, which is <strong><i>“Your personal growth, leads to organizational growth and transforms communities”</i></strong>. 
</p>
                </div>
            </div>
        </div>
             <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption about-us-box">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category" style="text-align:left" >
                                    Our Vision
                                </div>
                                <div >
                                <ul style="text-align:left">
                                    <li>To become The Associate of Individuals and organizations as they seek growth opportunities
                                    </li>
                                    <li>To lead transformation across sectors, communities and nations around the world
                                    </li>
                                    <li>To be the leading company and benchmark for state-of-the art Coaching services in Rwanda, Africa and globally
                                    </li>
                                    <li>To see people live their lives to their fullest potential</li>
                                    </ul>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box portfolio-box-caption">
                        <img src="img/portfolio/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption about-us-box">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category " style="text-align:left" >
                                   Our Mission
                                </div>
                                    <div >
                                <ul style="text-align:left">
                                <li>To translate people’s dreams into real terms through world<li>ss coaching tools</li>
                                <li>To restore Human Capital Development as core element of growth within organizations</li>
                                <li>To empower people, businesses and communities through establishment of a coaching & mentoring culture</li>
                                <li>To exceed expectation and constantly evolve and innovate our product and service offerings </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption about-us-box">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category "style="text-align:left">
                                    Our Core values
                                </div>
                                <div style="text-align:left">
                                 <ul>
                                    <li>To create</li>
                                    <li>To work or to do something </li>
                                    <li>To innovate, develop and bring forth</li>
                                    <li>To collaborate or work with people </li>
                                    <li>To inspire or touch   </li>
                                 </ul>
                                 <br>
                                 <br>
                                 <br>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- Button trigger modal -->
        <?php require 'about-cofounder-modal.php'; ?>