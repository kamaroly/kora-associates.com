<form class="form-horizontal">
    <div class="form-group">
        <label for="inputfirst_name" class="control-label col-xs-3">First name</label>
          <div class="col-xs-9">
            <input type="first_name" class="form-control" id="inputfirst_name" placeholder="John">
          </div>
    </div>
    <div class="form-group">
        <label for="inputlast_name" class="control-label col-xs-3">Last name</label>
          <div class="col-xs-9">
            <input type="last_name" class="form-control" id="inputlast_name" placeholder="Doe">
          </div>
    </div>
    <div class="form-group">
        <label for="inputcompany_name" class="control-label col-xs-3">Company name</label>
          <div class="col-xs-9">
            <input type="company_name" class="form-control" id="inputcompany_name" placeholder="Company name">
          </div>
    </div>
    <div class="form-group">
        <label for="inputjob_title" class="control-label col-xs-3">Job title</label>
          <div class="col-xs-9">
            <input type="job_title" class="form-control" id="inputjob_title" placeholder="Manager">
          </div>
    </div>
    <div class="form-group">
        <label for="inputtelephone" class="control-label col-xs-3">Telephone</label>
          <div class="col-xs-9">
            <input type="telephone" class="form-control" id="inputtelephone" placeholder="+250722000000">
          </div>
    </div>
    <div class="form-group">
        <label for="inputemail" class="control-label col-xs-3">Email address</label>
          <div class="col-xs-9">
            <input type="email" class="form-control" id="inputemail" placeholder="user@example.com">
          </div>
    </div>
    <div class="form-group">
        <label for="inputcity" class="control-label col-xs-3">City</label>
          <div class="col-xs-9">
            <input type="city" class="form-control" id="inputcity" placeholder="Kigali">
          </div>
    </div>
    <div class="form-group">
        <label for="inputcountry" class="control-label col-xs-3">Country</label>
          <div class="col-xs-9">
            <input type="country" class="form-control" id="inputcountry" placeholder="Rwanda">
          </div>
    </div>
    <div class="form-group">
        <label for="inputcomment" class="control-label col-xs-3">Comment</label>
          <div class="col-xs-9">
            <input type="comment" class="form-control" id="inputcomment" placeholder="What do you want to become">
          </div>
    </div>
    <div class="form-group">
    <label for="inputcv" class="control-label col-xs-3">Attach your cv</label>
          <div class="col-xs-9">
            <input type="file" class="form-control" id="inputcv" >
          </div>
    </div>
    <div class="form-group">
    <label for="inputtelephone" class="control-label col-xs-3">&nbsp;</label>
      <div class="col-xs-9">
         <button type="submit" class="btn btn-success col-xs-12">Apply</button>
      </div>
</div>
   
</form>