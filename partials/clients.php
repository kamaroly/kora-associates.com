<div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Clients</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
        <div class="col-md-6" style="border-right: 1px solid #909090;">
<p>
<h4>Our Clients, Partners and Sponsors</h4>
Collaboration is a core founding value. Since inception of KORA ASSOCIATES, 
now KORA COACHING GROUP, we value mutual collaboration with various stakeholders. <br>
Below is a select list from our extended Clients, Partners and Sponsors. 
The short list include both past and present organizations who have interacted with KORA one various projects.
<br>
</p>
<p>
<h4>Banks</h4>
IFC World Bank Group, International Monetary Fund (IMF), Development Bank of Rwanda (BRD), Ecobank, Equity Bank, KCB, BPR Atlas Mara, Cogebanque
<br></p>

          </div>  
<div class="col-md-6">
<p>
<h4>Corporations</h4>
New Faces New Voices – Rwanda ,IHS Towers, Africa Improved Foods (AIF), Bralirwa, RwandAir, AZAM, Touch Media, Water Access Rwanda, UZURI K&Y Design, Hobe Agency
</p>
<p>
<h4>Associations</h4>
PSF - Rwanda Chamber of Women Entrepreneurs (RCWE), The Women’s Information Network (WIN)
</p>
<h4>Individuals</h4>
Emerging Entrepreneurs
</p>

      </div>
    </div>
</div>