<div class="container-fluid" style="color: #000;background: #fff">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Coaches</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
        <div class="col-md-6" style="border-right: 1px solid #909090;">
        <p>
Global Research and studies show that a coaching culture improves employees’ engagement and companies retain workforce through talent management. The practice of internal or external coaching entails a large number of positive aspects within organizational Human Resources Management. Benefits of coaching are multifold. Below is a summary categorized in three essential areas:
<ul class="checkmark">
    <li><strong>Personal Development:  </strong> Through Coaching capabilities are harnessed as Talent is recognized and Performance is enhanced </li>
    <li><strong>Leadership Development: </strong> Leaders & Managers improve Leadership skills which directly impact individuals, employees & organizations</li>
    <li><strong>Human Capital Development:</strong> Consistent organizational coaching practice leads to effective personal and collective growth</li>
</ul>
</p>
<p>
As a Leading Coaching Company, we believe that Coaches play an integral part in the development in general.
One Coaching session with a Certified Coach can transform people’s lives and entire economies. With this in mind, KORA’s vision is to enable transformation through the setup of Coaching Courses specifically designed for Professional Coaches.
<br>
<strong>KORA Coaching Academy™</strong>, launching in the third quarter of 2017, will allow individuals to be trained as Professional Coaches. 
<br>
As a <a href="koracode.com" target="_blank"><strong>Certified KORA Code Coach™ (CKCC)</strong></a>, 
you will gain access to unparalleled core competencies through 5 coaching models. 
        </p>

          </div>  
      <div class="col-md-6">
	<p>
<strong>KORA Coaching Academy™ </strong>
courses are scheduled to run in two strategic locations of Africa<br>

> Kigali, Rwanda <br>
> Johannesburg, South Africa.
</P>
<p>
  Are you interested in becoming a <strong>Certified KORA Code Coach™ (CKCC)</strong>? Congratulations for your decision. Contact our team and we will walk your through the exciting journey  
</p>
<h4>Contact Us</h4>

<?php require 'contact-form.php'; ?>

      </div>
    </div>
</div>