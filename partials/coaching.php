
    <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Coaching</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
              <div class="col-md-6" style="background:#34495e;color: #fff;">
                  <p class="text-center">
                            Coaching is one of the core pillars of Kora-coaching services. We believe that Empowerment through effective coaching can have 
                            tremendous impact in business and personal growth. Our state-of-the-art coaching solutions are perfectly designed to meet 
                            individuals’ need for development either as a private person or within the corporate environment. Our range of coaching resources 
                            for individuals focus on the following areas: 
                        </p>
                     <p class="text-center">

                     <hr class="light">
                        KORA Code™ is the brand of uniquely selected array of coaching solutions designed by Kora-coaching.
            KORA Code™ all-encompassing set of leading coaching solutions allows companies and individuals to handpick a module that meets their needs at any 
            particular point in time. Unparalleled variety of world-class modular solutions such as KORA™ are tailored to facilitate personal development, 
            while growing interpersonal skills 
            and developing authentic relationships with friends, family, clients, business partners and others who are broadly connected to individuals or organizations.
            <br>
            <br>
                        </p>
              </div>
        <div class="col-md-6" style="background:#1884CD;color: #fff;padding: 10px;">        
        <div class="col-lg-12 text-center">
            <h4 class="section-heading">Personal Development </h4>
            <hr >
            <ul class="checkmark">
            <li >Mentoring</li>       
            <li >Life Coaching</li>         
            <li >Motivational Coaching</li>
            <li >Public events & Conferences</li>   
            </ul>             
        </div>
        
         <div class="col-lg-12 text-center">
         <br>
            <h4 class="section-heading">Organizational Coaching </h4>
            <hr >
            <ul class="checkmark">
            <li>Retreat workshops</li>
            <li>Executive Coaching</li>
            <li>Business Development</li>
            <li>Leadership Development </li>
            <li>Human Capital Management</li>
</ul>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12 text-center">
        <a href="http://koracode.com" class="btn btn-success btn-xl page-scroll" target="_blank">Find out more</a>
        </div>  
    </div>
</div>
