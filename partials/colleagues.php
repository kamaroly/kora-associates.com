<div class="container-fluid" style="color: #000;background: #fff">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Colleagues</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
        <div class="col-md-12" style="border-right: 1px solid #909090;">
        <p>
        <img src="/img/mireille-karera-2.png" class="img col-xs-2">
<strong>Ineza Mireille Karera</strong>is Group CEO & <strong>Lead Coach at KORA Coaching Group</strong>, 
formerly known as Kora Associates. KORA is a Coaching and Consulting company which originated in Dubai, United Arab 
Emirates, now present in Rwanda and South Africa. Mireille is Rwandan. <strong>She speaks 7 languages</strong> 
fluently (English, French, German, Rwandan, Swahili, Burundian and Lingala). 
In her earlier years, she pursued her passion for languages which earned her a Bachelor Degree in Business & Legal 
Translation in Cologne, Germany. 
Her language skills set opened up new avenues and a career in Financial Services in London, UK. 
</p>
<p>
She has spent over <strong>13 years working for Global Organizations</strong> such as Moody’s Investor Services, EBS 
Dealing Resources (now ICAP) and Thomson Reuters in London, Dubai, Johannesburg and Nairobi. Her clients ranged from 
Financial Institutions to Governments, Corporations and Trading Houses in Capital Markets, Commodities such as Oil & Gas 
and petrochemicals across Europe Middle East & Africa. Mireille has held a number of key roles in Sales, Business 
Development, Growth Strategy in Emerging Markets, including Coaching and Personal Development.  Her passion for Personal 
Development led her to pursuing further qualifications and became a Certified Professional Coactive Coach (CPCC) from the 
Coaches Training Institute (CTI)  member of the International Coaching Federation (ICF). CTI is among proud sponsors of the 
Institute of Coaching at McLean Hospital, an affiliate of Harvard Medical School. 
</p>
<p>
Mireille has acted as a Chairperson of a Dubai based Women’s Network which empowers Women in Leadership roles and 
development in the workplace. <strong>She has contributed to the setup of professional mentoring programs for women in the 
Middle East and Africa</strong>.  Mireille is a member of C3 (Consult and Coach for a Cause) a Dubai-based professional 
network promoting 
Social Entrepreneurship. Her vision and mission as a Coach & Consultant is to empower individuals, businesses and 
communities in creating and developing growth plans.  Mireille is passionate about imparting her enthusiasm to people. Part 
of her Community Advisory activities include being the Event <strong>Director of Global Women’s Summits</strong>, 
which aim at <strong>Empowering</strong> women through <strong>Education</strong> and <strong>Entrepreneurship</strong>. 
</p>

<p>
  Mireille is a philanthropist at heart. She is co-founder and former Managing Director of a non-profit charitable 
  organization which supports orphaned and disadvantaged children in Burundi and Rwanda. Her Life’s Mission is to be the 
  Ray of Hope to individuals, businesses and communities with a belief that the future is bright. She sees herself as a 
  pioneer, with a track record of breaking barriers into new frontiers. 
</p>

<p>
  She has reinvented her career 3 times, thus seeking to empower people to come out of their comfort zone by leading a fulfilling life. Her current journey and passion revolve around <strong>empowering a generation of World-Class Business Leaders who will transform Africa into an economic powerhouse</strong>. Mireille’s wish is to see <strong>more successful women leaders</strong>, entrepreneurs and inspirational public speakers come onto the global arena. 
</p>

<p>
<h4>In 2017, Ineza Mireille Karera was awarded Africa’s Top 25 Distinguished Women in Business Excellence by 
Amazons Watch Magazine & Center for Economic and Leadership Development (CELD) </h4>
<img src="/img/amazon-watch.png" class="img col-xs-6">
<div class="col-xs-6">
<h3 style="text-align: center;">Application Form</h3>
To apply for a career at KORA Coaching Group, please fill in the form below with your CV

  <?php require 'application-form.php'; ?>
</div>
</p>




      </div>
    </div>
</div>