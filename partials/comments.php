<div class="container-fluid" style="color: #000;background: #fff">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Comments and Testimonials</h2>
                <hr class="primary">
              <p>
Have you been impacted by our Coaching, conferences, events or workshops? 
Join the conversation and send us your comments!</p>
<p>This is what our Clients, Partners and Sponsors say about our collaboration:</p>
            </div>
        </div>
        <div class="row">
        <div class="col-md-6" style="border-right: 1px solid #909090;">
        <p>
<i>
“Mireille became an important contributor for a big media project I was undertaking at the time.
As a coach she found the right balance between encouraging and challenging.
Having lived and worked in many countries and cultures, she is also capable of coaching with a multifaceted perspective. Her excellent written summary with action points turned out to be a very helpful navigation tool. To sum it up: The coaching process with her is highly inspiring, very enjoyable and most importantly, delivers positive results.”
</i>
<br>
<a href="#"><strong>- Rainer Michael Flick, Media Composer & Producer – Switzerland</strong></a>
  </p>
<p>
<i>
“We had a wonderful time working with our coach and would recommend her sessions to any entrepreneur or leader from a starter to an experienced person in their field of work. The coaching method is user friendly, easy and would be easily adapted by any person willing to work.” 
</i>
<br>
<a href="#"><strong> – Ysolde Ishimwe, CFO at Uzuri K&Y - Rwanda</strong></a>
  </p>
<p>
<i>
“My grateful thanks to KORA Associates and the team for the superb organization behind the success of the Global Women’s Summit -Rwanda. This was a major international event demanding meticulous planning and attention to detail. I loved the energy, the patience exercised by the women, it was indeed a great summit with so much to learn about.” 
</i>
<br>
<a href="#"><strong> – Fidelis Wanjiku - Kenya</strong></a>
  </p>

</div>  
<div class="col-md-6">
<p>
<i>
“During my coaching with Coach Mireille I had the privilege of learning so much but my take away
was that I should have a well-balanced life that is Career, Finances, Health, Social life, Spiritual and
Love/relationship and the step by step process to achieve it. There was also the practical training of
the hard place which is the area of struggle and the place of comfort which I named my paradise and
the process of getting there.” 
 
</i>
<br>
<a href="#"><strong>- Mary Otogo – Dubai, UAE</strong></a>
  </p>

<p>
<i>
“Encouraged to step out. Working with Mireille helped me to find confidence in myself and my God given talents and abilities. I was able to narrow down and focus in on the areas that God wants to use me in. I was able to start my first business Papillion and seen it touch and bless other lives. I believe this to be but the start. The coaching I received has helped me to experience success which encourages me to keep going. I now see an infinite possibilities for my future and I have the courage to dream bigger. “
</i>
<br>
<a href="#"><strong>- Amy Uys  South Africa</strong></a>
  </p>

      </div>
    </div>
</div>