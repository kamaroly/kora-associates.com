
<div class="container text-center">
    <div class="row">
        <h2 class="section-heading">Community</h2>
        <hr class="light">
    </div>
    <div class="call-to-action">
        <h3>Social Mission</h3>
        <p>
          Corporate Social Responsibility is one of our foundational principles. Our Social Mission is centered on Empowerment of Individuals, business and 
          communities where we do business. Our Vision is to become "Associates" of communities by creating an environment where enterprises are created,
           local businesses see growth through creation of new employment avenues and development of diverse sectors. Each community has needs and 
           opportunities which our diverse yet unique in nature. We recognize and value the wealth of possibilities which each community has to offer
        </p>
        <p>Local Governing Authorities, key Social Enterprises are our partners in identification and implementation of Socially Responsible projects. </p>
        <hr>
       <h3>Social Vision</h3>
        <p>
         Our “Social Vision” is to see schools & hospitals built, SMEs grow and local heritage preserved in developing countries.
        We support philanthropic projects of charitable organisation such as  “Plebis Foundation” which  raises funds for basic and Education needs of 
        orphans and disadvantaged children.

        </p>
        <p>
        As a company, KORA has a heart for giving back to communities. Our Corporate Social Responsible Business line include activities in: 
        <div class="btn btn-primary" style="cursor: inherit;">
<div class="btn" style="cursor: inherit;">Women Economic Empowerment</div><br>
<div class="btn" style="cursor: inherit;">Business Associations Advisory </div><br>
<div class="btn" style="cursor: inherit;">Philanthropic Advisory</div>
</div>
</p>
        <hr>
    </div>
</div>
