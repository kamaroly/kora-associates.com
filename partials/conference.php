<div class="container-fluid" style="color: #000;background: #fff">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Conferences</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
        <div class="col-md-6">
        <p>
KORA Coaches are often sought as Conference Speakers or Consultants for organization of key workshops and flagship events. Our past conference, workshops, retreat activities include advisory in the following areas:
<ul class="checkmark">
    <li><strong>Concept Development:</strong>  designing content for specific conferences with Personal & Organizational growth as theme</li>
    <li><strong>Corporate Retreats Facilitation:</strong> Facilitation of Corporate retreats, team building activities to name a few</li>
    <li><strong>Classroom Workshop:</strong> running of public courses</li>
</ul>
</p>
<p>
Our Past participation included invitations to attend conferences as guests or organizers. Below is a sample list of various events run or co-run by KORA.

        </p>

          </div>  
      <div class="col-md-6" style="border-left: 1px solid #909090;">
	<p>
<h3>2017</h3>
<a href="https://drive.google.com/file/d/0B0mQbIOm7vD_UmNENkpERy05R2s/view?usp=sharing" target="_blank"> <strong><u>Canopy Breakfast</u></strong></a><br>
KORA CEO invited to talk on a panel discussion around the theme “Godly values in the Marketplace” <br>
<a href="https://drive.google.com/file/d/0B0mQbIOm7vD_RUh5YWVNZTZoek0/view?usp=sharing" target="_blank"> <strong><u>Forum on African Women Leadership Development</u></strong></a><br>
KORA presented its core coaching model for Leadership Development: <strong><i>Leaders L.I.F.T Leaders™</i></strong><br>
<a href="http://www.amazonswatchmagazine.com/conferences/women-finance-summit-2017/" target="_blank"> <strong><u>Women & Finance Summit</u></strong></a><br>
KORA lead a coaching session on flagship KORA Code™ model <strong><i>7Minutes Management Mode</i></strong>  <br>
<h3>2016</h3>
<a href="https://drive.google.com/file/d/0B0mQbIOm7vD_MzBKNU9jc3ZoanM/view?usp=sharing" target="_blank"> <strong><u>Advanced Certificate in Strategic Procurement (ACSP™)</u></strong></a><br>
Public Workshop Delivering Success through Effective Procurement and Supply Chain Management. <br>
<a href="https://www.facebook.com/GlobalWomenSummitRwanda/" target="_blank"> <strong><u>Global Women’s Summit- Rwanda</u></strong></a><br>
KORA acted as Event’s Director of 1st Global Women’s Summits in Rwanda, presented by The Women Information <br>Network (WIN) <br>
<h3>2015</h3>
<a href="https://youtu.be/cMYrbYXsO8w" target="_blank"> <strong><u>AFAB Coaching Seminar </u></strong></a><br>
KORA partnered with the Association of Business Women in Burundi to bring key coaching tool to Business women of Burundi 
</p>
      
</P>

      </div>
    </div>
</div>