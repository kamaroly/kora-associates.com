<form class="form-horizontal">
    <div class="form-group">
        <label for="inputfirst_name" class="control-label col-xs-2">First name</label>
          <div class="col-xs-10">
            <input type="first_name" class="form-control" id="inputfirst_name" placeholder="John">
          </div>
    </div>
    <div class="form-group">
        <label for="inputlast_name" class="control-label col-xs-2">Last name</label>
          <div class="col-xs-10">
            <input type="last_name" class="form-control" id="inputlast_name" placeholder="Doe">
          </div>
    </div>
    <div class="form-group">
        <label for="inputcompany_name" class="control-label col-xs-2">Company name</label>
          <div class="col-xs-10">
            <input type="company_name" class="form-control" id="inputcompany_name" placeholder="Company name">
          </div>
    </div>
    <div class="form-group">
        <label for="inputjob_title" class="control-label col-xs-2">Job title</label>
          <div class="col-xs-10">
            <input type="job_title" class="form-control" id="inputjob_title" placeholder="Manager">
          </div>
    </div>
    <div class="form-group">
        <label for="inputtelephone" class="control-label col-xs-2">Telephone</label>
          <div class="col-xs-10">
            <input type="telephone" class="form-control" id="inputtelephone" placeholder="+250722000000">
          </div>
    </div>
    <div class="form-group">
    <label for="inputtelephone" class="control-label col-xs-2">&nbsp;</label>
      <div class="col-xs-10">
         <button type="submit" class="btn btn-success col-xs-12">Contact us</button>
      </div>
</div>
   
</form>