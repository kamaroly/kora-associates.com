
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 text-center">
            <h2 class="section-heading">Connect </h2>
            <hr class="primary">
            <p>For more information, please feel free to connect with one of our Associates on contacts below</p>
        </div>
        <div class="col-lg-8 col-lg-offset-2 text-center">
            <h3 class="section-heading">Our Offices  </h3>
        </div>
        <div class="col-lg-4 col-lg-offset-2">
         <h2 class="section-heading"><u>Rwanda</u></h2>
            <p><strong>KORA Coaching Group Ltd.</strong>  <br>
            KK 35 Ave Kigarama  <br>
            Kicukiro, Kigali <br>
            P.O. Box 5007  <br>
            Rwanda <br>
            Contact details <br>
            Tel : +250782494473 <br>
            Email: info@kora-coaching.com</p>

        </div>
        <div class="col-lg-4 col-lg-offset-2">
         <h2 class="section-heading"><u>South Africa</u></h2>
            <p><strong>KORA Coaching Group (pty) Ltd. </strong>  <br>
            111 Kelvin Drive <br>
            Unit 11, Sandton <br>
            Morningside 2196 <br>
            South Africa  <br>
            Contact details <br>
            Tel : +27636335384 <br>
            Email: info@kora-coaching.com</p>

        </div>
    </div>
    <div class="row text-center">
        <p>Our office Hours: Monday to Friday: 8: 30 am - 5:30 pm (GMT+2)</p>
    </div>
</div>