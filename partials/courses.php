<div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Courses</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
        <div class="col-md-6" style="border-right: 1px solid #909090;">
        <p>
        In addition to KORA Code, proprietary coaching services, we have extended our service offering
with a new range of first class training courses. Kora-coaching Ltd. teamed up with Innoverto
FZE, a United Arab Emirates based company and leading provider of unparalleled top notch in-
house and public certification courses. The partnership between Innoverto and KORA is a game changer for Rwanda and Africa.
Combined organizations aim at providing services that will transform Africa’s Best Practices for Professionals on the Continent with Internationally Recognized Certification Programs.
        </p>
	<p>
	<a href="http://www.innoverto.com/" target="_blank"><img src="/img/Innoverto-logo.png" align="left" style="width:300px; padding:10px;"></a>
       <h4>About Innoverto FZE</h4>
		Innoverto is a Corporate Training and Events Management Company based in the United Arab Emirates. Innoverto offering, among many include high quality business trainings, including 
		certified courses throughout the Middle East and Africa. Innoverto courses are rendered in various formats such as public or in-house courses which are tailored according to business 
		needs for individuals and teams. As an Events Management Company, Innoverto specializes in bringing world-class speakers to the region for lectures and seminars, as well as timely and 
		topical industry events or conferences. 
		<br/>
		<br/>
		For more information please visit:<a href="http://www.innoverto.com/" target="_blank">www.innoverto.com </a>
        </p>
          </div>  
      <div class="col-md-6">
      <h4>The courses scheduled between October - December are:</h4>
<P>
We are delighted to announce the launch of Innoverto and international certification courses in Kigali, Rwanda with 5 courses scheduled for the fourth quarter of 2016.
	<div  class="list-group">
<a href="http://cts.vresp.com/c/?InnovertoFZLLC/3be6bc939e/dc701aafe8/c9f9197391" class="list-group-item" target="_blank">NYIM Accelerated Mini MBA program <!-- <span class="badge">24-28 October, 2016</span> --></a> 
<a href="http://cts.vresp.com/c/?InnovertoFZLLC/3be6bc939e/dc701aafe8/1a8ee9ee28" class="list-group-item" target="_blank">Effective Data Analysis For Successful Business Decisions <!-- <span class="badge">1-4 November, 2016</span> --></a> 
<a href="http://cts.vresp.com/c/?InnovertoFZLLC/3be6bc939e/dc701aafe8/ac0c7a37bc" class="list-group-item" target="_blank">Advanced Certificate in Strategic Procurement (ACSP)<!--  <span class="badge">21-25 November, 2016 </span> --></a> 
<a href="http://cts.vresp.com/c/?InnovertoFZLLC/3be6bc939e/dc701aafe8/af32f54c62" class="list-group-item" target="_blank">Advanced Certificate for the Executive Personal Assistant (ACEPA) <!-- <span class="badge">5-9 December, 2016 </span> --></a>
<a href="http://cts.vresp.com/c/?InnovertoFZLLC/3be6bc939e/dc701aafe8/5921e5329d" class="list-group-item" target="_blank">Advanced Certificate Operational Management (ACOM)  <!-- <span class="badge">12-16 December, 2016 </span> --></a>
</div>
<strong>Public Courses Location:</strong> Kigali, Rwanda and Johannesburg, South Africa <br/>
<!--  Kigali, Rwanda and Johannesburg, South 
Rwanda being announced as one of Africa’s fastest growing economies and most secure place in Africa, it is only a natural choice to have the capital, Kigali as the base for our upcoming international courses. -->
</P>

      </div>
    </div>
</div>