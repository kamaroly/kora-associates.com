<div class="container text-center">
         <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Coverage</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
      <div class="col-md-12">
	<p>
  KORA Associates and KORA Coaching Group’s activities are covered in various media across Africa.
Our Achievements and Awards acknowledgements can be found on the links below <br>

<a href="http://www.amazonswatchmagazine.com/africas-top-25-distinguished-women-in-business-excellence-2017" target="_blank"><strong><u>Africa’s Top 25 Distinguished Women in Business Excellence</u></strong></a> <br>
Amazon Watch Magazine - Nigeria <br>
<a href="http://www.amazonswatchmagazine.com/africas-top-25-distinguished-women-in-business-excellence-2017" target="_blank"><strong><u>Mireille Karera yahembwe muri ba rwiyemezamirimo 25 b’indashyikirwa muri Afurika </u></strong></a> <br>
Igihe,com – Rwanda <br> 
<a href="http://www.lionessesofafrica.com/blog/2016/1/28/the-startup-story-of-mireille-karera" target="_blank"><strong><u>The Startup Story of a Rwandan Entrepreneur</u></strong></a> <br>
Lionesses of Africa – South Africa  <br>
<a href="https://youtu.be/Dfl4svq6Zd4" target="_blank"><strong><u>CNBC Interview with Mireille Karera</u></strong></a> <br>
CNBC - Rwanda <br>
<iframe width="560" height="315"  src="https://www.youtube.com/embed/Dfl4svq6Zd4?ecver=1" frameborder="0" allowfullscreen></iframe> <br>
<a href="http://leadingladiesafrica.org/site/tag/mireille-karera/" target="_blank"><strong><u>Meet Mireille, the Social Entrepreneur</u></strong></a> <br>
Leading Ladies of Africa – Nigeria <br>
<a href="http://en.igihe.com/news/introducing-business-management-coaching-for.html" target="_blank"><strong><u>Introducing business management coaching for transforming Africa’s best practices, professional development</u></strong></a> <br>
Igihe - Rwanda <br>
<a href="http://www.newtimes.co.rw/section/article/2016-03-08/197771/" target="_blank"><strong><u>Global Women's Summit opens in Kigali</u></strong></a> <br>
New Times – Rwanda  <br>
<a href="http://allafrica.com/stories/201603080128.html" target="_blank"><strong><u>Africa: Global Women's Summit Opens in Kigali</u></strong></a> <br>
AllAfrica – Kenya <br>
<a href="https://youtu.be/EWojlifa5UM" target="_blank"><strong><u>Debate 411: International Women's Day Summit</u></strong></a> <br>
Rwanda Broadcasting <br> Agency (RBA) – Rwanda
To request an interview with our Leadership team, please contact us with your enquiry.
</p>
      </div>
    </div>
</div>