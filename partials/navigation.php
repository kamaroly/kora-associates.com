<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top" href="http://koracode.com/"> 
                <img src="img/logo.png" alt=""> </a>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#about">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#coaching">Coaching</a>
                </li>
                <li>
                    <a class="page-scroll" href="#consulting">Consulting</a>
                </li>
                  <li>
                    <a class="page-scroll" href="#community">Community</a>
                </li>
                <li>
                    <a class="page-scroll" href="#courses">Courses</a>
                </li>
                <li>
                    <a class="page-scroll" href="#coaches">Coaches</a>
                </li>
                <li>
                    <a class="page-scroll" href="#conference">Conference</a>
                </li>
                <li>
                    <a class="page-scroll" href="#coverage">Coverage</a>
                </li>
                <li>
                    <a class="page-scroll" href="#colleagues">Colleagues</a>
                </li>
                <li>
                    <a class="page-scroll" href="#comments">Comments</a>
                </li>
                <li>
                    <a class="page-scroll" href="#clients">Clients</a>
                </li> 
                <li>
                   <a class="page-scroll" href="#contact">Connect</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>