<header>
    <div class="header-content">
        <div class="header-content-inner">
        <div class="row">
        <div class="col-md-10" >
            <p style="font-size: 40px;font-weight: 100;
	    text-transform: uppercase;
	     margin-top: 150px;
	    margin-bottom: 0;
	    color:#00C9FF !important;">Innovating,Creating, Working,Developing.</p>
            <hr>
            <p>The expression "KORA"  translates into "doing, creating, acting, working,  realising, generating, innovating, bringing forth and developing"
             - all  attributes perfectly describing Kora-coaching.</p>
            <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
        </div> 
            <div class="col-md-2" >
            <a href="#courses" class="btn page-scroll">
             <img src="https://upload.wikimedia.org/wikipedia/commons/c/c4/600_px_Transparent_flag.png" style="width:100%;height:100%;">
            </a>
           </div> 
         </div> 
        </div>  
    </div>
</header>